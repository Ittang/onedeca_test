import React, { Component } from 'react';
import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator
} from 'react-navigation';
import Icon from 'react-native-vector-icons/Feather';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
//root screen
import LoginScreen from './src/LoginScreen';
//admin screen
import DashboardAdmin from './src/admin/Dashboard';
import Payment from './src/admin/Payment';
import AddProduct from './src/admin/component/AddProduct';
import EditProduct from './src/admin/EditProduct';
import ListItem from './src/admin/component/ListItem';
//user screen
import DashboardUser from './src/user/Dashboard';
import Cart from './src/user/Cart';
import Checkout from './src/user/Checkout';
import Thankspage from './src/user/Thankspage';
import ListItemUser from './src/user/component/ListItem';

//REDUX
import store from './src/store';
import { Provider } from 'react-redux';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    )
  }
}

const AppBottomNavigator = createMaterialBottomTabNavigator({
  DashboardUser: {
    screen: DashboardUser,
    navigationOptions: {
      title: 'Beranda',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="home" size={24} color={tintColor} />
      )
    }
  },
  Cart: {
    screen: Cart,
    navigationOptions: {
      title: 'Transaksi',
      tabBarIcon: ({ tintColor }) => (
        <Icon name="truck" size={24} color={tintColor} />
      )
    }
  }
}, {
    initialRouteName: 'DashboardUser',
    activeTintColor: 'black',
    inactiveTintColor: 'gray',
    barStyle: {
      backgroundColor: 'white',
    }
  })

const UserStackNavigator = createStackNavigator({
  AppBottomNavigator: {
    screen: AppBottomNavigator,
    navigationOptions: {
      header: null
    }
  },
  Checkout: {
    screen: Checkout,
    navigationOptions: {
      title: "Checkout"
    }
  },
  Thankspage: {
    screen: Thankspage,
    navigationOptions: {
      header: null
    }
  },
  ListItemUser: {
    screen: ListItemUser,
    navigationOptions: {
      header: null
    }
  },
})

const AppStackNavigator = createStackNavigator({
  DashboardAdmin: {
    screen: DashboardAdmin,
    navigationOptions: {
      header: null
    }
  },
  Payment: {
    screen: Payment,
    navigationOptions: {
      title: "Transaksi"
    }
  },
  AddProduct: {
    screen: AddProduct,
    navigationOptions: {
      title: "Tambah produk"
    }
  },
  EditProduct: {
    screen: EditProduct,
    navigationOptions: {
      title: "Edit produk"
    }
  },
  ListItem: {
    screen: ListItem,
    navigationOptions: {
      header: null
    }
  },
})

const AppSwitchNavigator = createSwitchNavigator({
  LoginScreen: { screen: LoginScreen },
  DashboardAdmin: { screen: AppStackNavigator },
  DashboardUser: { screen: UserStackNavigator }
})

const AppContainer = createAppContainer(AppSwitchNavigator)
// const AppContainer = createAppContainer(AppStackNavigator)
// const AppContainer = createAppContainer(UserStackNavigator)