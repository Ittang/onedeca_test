import React, {Component} from 'react';
import {View,Text,StyleSheet, Dimensions,AsyncStorage, ToastAndroid} from 'react-native';
import {TextInput,Button} from 'react-native-paper';
import Modal from 'react-native-modalbox';

var screen = Dimensions.get('window');
export default class RegisterModal extends Component {
    constructor(props){
        super(props)
        this.state = {
            inputUsername: '',
            inputPassword: ''
        }
    }

    showModal=()=>{
        this.refs.myModal.open();
    }
    closeModal=()=>{
        this.refs.myModal.close();
    }

    registerSave=()=>{
        const {inputUsername,inputPassword} = this.state;
        if(inputUsername.trim() === ""){
            ToastAndroid.show('Mohon isi username', ToastAndroid.SHORT);
            return;
        } else if(inputPassword.trim() === ""){
            ToastAndroid.show('Mohon isi password', ToastAndroid.SHORT);
            return;
        } else {
            try {
                AsyncStorage.setItem('admin:username', inputUsername);
                AsyncStorage.setItem('admin:password', inputPassword);
                ToastAndroid.show('Sucessfully Register', ToastAndroid.SHORT);
                this.refs.myModal.close()
            } catch (error) {
                alert(error)
            }
        }
    }

    render(){
        return(
            <Modal 
                ref={"myModal"}
                style={styles.container}
                position="center"
                backdrop={true} >
                <Text style={styles.registerText}>Register admin</Text>
                <View style={{marginTop:32}}>
                    <TextInput
                        onChangeText={inputUsername => this.setState({inputUsername})}
                        placeholder="Insert username"
                        mode="outlined" />
                    <TextInput 
                        onChangeText={inputPassword => this.setState({inputPassword})}
                        mode="outlined"
                        secureTextEntry={true}
                        placeholder="Insert password" />
                    <Button onPress={this.registerSave} mode="contained" style={{marginTop:10, padding:8}}>
                        Register
                    </Button>
                </View>
            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        shadowRadius: 10,
        justifyContent:'center',
        borderRadius: 10,
        width: screen.width - 80,
        height: 300,
        padding: 16
    },
    registerText: {
        fontSize: 24,
        fontWeight: 'bold'
    },
})