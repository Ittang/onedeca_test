import React, { Component } from 'react';
import {
    View, Text, StyleSheet, TouchableOpacity,
    AsyncStorage, ToastAndroid
} from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import RegisterModal from './RegisterModal';


export default class LoginScreen extends Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            password: '',
            inputUsername: '',
            inputPassword: '',
        }
    }

    componentDidMount = () => {
        AsyncStorage.getItem('admin:username').then((value) => this.setState({ username: value }))
        AsyncStorage.getItem('admin:password').then((value) => this.setState({ password: value }))
    }

    registerAdmin() {
        this.refs.registerModal.showModal();
    }

    login = () => {
        const { inputUsername, inputPassword } = this.state;
        const myUsername = this.state.username;
        const myPassword = this.state.password;

        if (inputUsername == "" & inputPassword == "") {
            ToastAndroid.show('Mohon isi username dan password', ToastAndroid.SHORT)
        }
        else if (inputUsername == "") {
            ToastAndroid.show('Username kosong', ToastAndroid.SHORT)
        }
        else if (inputPassword == "") {
            ToastAndroid.show('Password kosong', ToastAndroid.SHORT)
        }
        else if (inputUsername != myUsername && inputPassword != myPassword) {
            ToastAndroid.show('Username atau password salah', ToastAndroid.SHORT)
        }
        else if (inputUsername == myUsername && inputPassword == myPassword) {
            this.props.navigation.navigate('DashboardAdmin')
        }
        else {
            ToastAndroid.show('Data tidak ditemukan', ToastAndroid.SHORT)
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.loginText}>Login as admin</Text>
                <View style={{ marginTop: 32 }}>
                    <TextInput
                        onChangeText={inputUsername => this.setState({ inputUsername })}
                        placeholder="Username"
                        mode="flat" />
                    <TextInput
                        secureTextEntry={true}
                        onChangeText={inputPassword => this.setState({ inputPassword })}
                        style={{ marginTop: 5 }}
                        placeholder="Password"
                        mode="flat" />
                    <Button uppercase={false} mode="outlined" color="tomato"
                        style={{ borderColor: "tomato", marginTop: 10, padding: 6 }}
                        onPress={this.login}>
                        Login
                    </Button>
                    <Button uppercase={false} mode="outlined" color="tomato"
                        style={{ borderColor: "tomato", marginTop: 10, padding: 6 }}
                        onPress={this.registerAdmin.bind(this)}>
                        Register as admin
                    </Button>
                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DashboardUser')}>
                        <Text style={styles.guestText}>
                            Continue as guest
                        </Text>
                    </TouchableOpacity>
                </View>
                <RegisterModal ref={"registerModal"} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 16,
    },
    loginText: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    guestText: {
        textAlign: 'right',
        marginTop: 16,
        fontSize: 16,
        fontWeight: 'bold'
    }
})