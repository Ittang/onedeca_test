import {
    ADD_PRODUCT, REMOVE_PRODUCT,
    UPDATE_PRODUCT, ADD_TO_CART
} from './actionTypes'

let nextId = 0

export const addProduct = (productName, productDescription, productPrice, productImage) => ({
    type: ADD_PRODUCT,
    id: nextId++,
    productName,
    productDescription,
    productPrice,
    productImage
})

export const removeProduct = (id) => ({
    type: REMOVE_PRODUCT, id
})

export const updateProduct = (id, data) => ({
    type: UPDATE_PRODUCT, id, data
})

export const addToCart = (data) => ({
    type: ADD_TO_CART, data
})