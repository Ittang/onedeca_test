import {
    ADD_TO_CART, ADD_PRODUCT,
    REMOVE_PRODUCT, UPDATE_PRODUCT,
} from '../actions/actionTypes'

const products = (state = [], action) => {
    switch (action.type) {
        case ADD_PRODUCT:
            return [
                ...state, {
                    id: action.id,
                    name: action.productName,
                    description: action.productDescription,
                    price: action.productPrice,
                    image: action.productImage,
                    completed: false
                }
            ]

        case REMOVE_PRODUCT:
            return state.filter((product, i) => i !== action.id)

        case UPDATE_PRODUCT:
            return state.map((product) => {
                if (product.id === action.id) {
                    return {
                        ...product,
                        name: action.data.newProductName,
                        description: action.data.newProductDescription,
                        price: action.data.newProductPrice,
                        imag: action.data.newProductImage
                    }
                } else return product
            })

        case ADD_TO_CART:
            return [...state, action.payload]
    }
    return state
}

export default products