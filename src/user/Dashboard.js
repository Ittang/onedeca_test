import React, { Component } from 'react';
import {
    View, StyleSheet, Text,
} from 'react-native';
import VisibleProduct from './container/VisibleProduct'

export default class Dashboard extends Component {

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>Nikmati berbagai produk kami</Text>
                <VisibleProduct navigation={this.props.navigation} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    text: {
        margin: 16,
        fontSize: 18,
        fontWeight: 'bold'
    }
})