import React, { Component } from 'react';
import {
    View, StyleSheet, Image, Dimensions, Text, BackHandler, Alert
} from 'react-native';
import { Button } from 'react-native-paper';
import {connect} from 'react-redux';

let screen = Dimensions.get('window')
class Thankspage extends Component {
    constructor(props){
        super(props)
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBack)
    }
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBack)
    }
    handleBack = () => {
        return true;
    }

    goToCart=()=>{
        const id = this.props.navigation.getParam('id', 'NO-ID')
        const productName = this.props.navigation.getParam('name', 'NO-ID')
        const productDescription = this.props.navigation.getParam('description', 'NO-ID')
        const productPrice = this.props.navigation.getParam('price', 'NO-ID')
        const productImage = this.props.navigation.getParam('image', 'NO-ID')
        // this.props.dispatch(addToCart(
        //     id, productName, productDescription,
        //     productPrice,productImage
        // ))
        this.props.navigation.navigate('Cart', {
            id,productName,productDescription,productPrice,productImage
        })
    }

    render() {
        const id = this.props.navigation.getParam('id', 'NO-ID')
        const productName = this.props.navigation.getParam('name', 'NO-ID')
        const productDescription = this.props.navigation.getParam('description', 'NO-ID')
        const productPrice = this.props.navigation.getParam('price', 'NO-ID')
        const productImage = this.props.navigation.getParam('image', 'NO-ID')
        return (
            <View style={{ flex: 1, paddingBottom:8 }}>
                <View style={styles.container}>
                    <Text style={styles.text}>Thanks you for purchasing</Text>
                    <Image style={styles.image} source={require('../admin/images/paymentsuccess-.png')} />
                    <Text style={styles.textProduk}>Product information</Text>
                    <View style={styles.containerBox}>
                        <Image
                            style={styles.imageBox}
                            source={{ uri: JSON.stringify(productImage) }} />
                        <View style={{ flexDirection: 'column', flex: 1, paddingHorizontal: 8 }}>
                            <Text style={styles.textProduk}>
                                {productName}
                            </Text>
                            <Text style={styles.textDeskripsi}>
                                {productDescription}
                            </Text>
                            <Text style={styles.textHarga}>
                               Rp. {productPrice}
                            </Text>
                        </View>
                    </View>
                </View>
                <Button mode="contained" color="#50A79F" uppercase={false} dark={true}
                    style={{ pading: 5, margin: 8 }}
                    onPress={this.goToCart}>
                    Lihat transaksi
                </Button>
            </View>

        )
    }
}

export default connect()(Thankspage)

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    text: {
        alignItems: 'flex-start',
        justifyContent: 'flex-start',
        fontSize: 24,
        fontWeight: 'bold'
    },
    image: {
        marginVertical: 8,
        height: 80, width: 80
    },
    views: {
        width: screen.width - 50,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 6,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8
    },
    containerBox: {
        flexDirection: 'row',
        borderRadius: 10,
        marginTop: 8,
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 16
    },
    imageBox: {
        borderWidth: 1,
        borderColor: 'white',
        height: 50,
        width: 50
    },
    textProduk: {
        fontSize: 16,
        color: 'gray',
        fontWeight: 'bold'
    },
    textDeskripsi: {
        fontSize: 14,
        color: 'gray'
    },
    textHarga: {
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        color: 'gray',
    }
})