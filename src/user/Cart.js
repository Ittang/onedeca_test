import React, { Component } from 'react';
import {
    View, Text, StyleSheet, Image
} from 'react-native';
import {Divider} from 'react-native-paper';

export default class Cart extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={styles.container}>
                    <Image
                        style={styles.image}
                        source={require('../admin/images/image.png')} />
                    <View style={{ flexDirection: 'column', flex: 1, paddingHorizontal: 8 }}>
                        <Text style={styles.textProduk}>
                            Nama produk
                        </Text>
                        <Text style={styles.textDeskripsi}>
                            Deskripsi produk
                        </Text>
                        <Text style={styles.textHarga}>
                            Rp. Harga produk
                        </Text>
                    </View>
                </View>
                <Divider style={{ marginHorizontal: 16 }} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderRadius: 10,
        marginTop: 8,
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 16
    },
    image: {
        borderWidth: 1,
        borderColor: 'white',
        height: 50,
        width: 50
    },
    textProduk: {
        fontSize: 16,
        color: 'gray',
        fontWeight: 'bold'
    },
    textDeskripsi: {
        fontSize: 14,
        color: 'gray'
    },
    textHarga: {
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        color: 'gray',
    }
})