import React, { Component } from 'react';
import {
    View, Text, StyleSheet, Image,
    ToastAndroid, Dimensions, ScrollView
} from 'react-native';
import { Button, Divider } from 'react-native-paper'
import { connect } from 'react-redux';
import { addToCart } from '../actions';

export default class Checkout extends Component {
    constructor(props) {
        super(props)
    }

    state = {
        products: []
    }

    btnAddToCart = () => {
        const id = this.props.navigation.getParam('id', 'NO-ID')
        const productName = this.props.navigation.getParam('name', 'NO-ID')
        const productDescription = this.props.navigation.getParam('description', 'NO-ID')
        const productPrice = this.props.navigation.getParam('price', 'NO-ID')
        const productImage = this.props.navigation.getParam('image', 'NO-ID')
        // this.props.dispatch(addToCart,(data))
        this.props.navigation.navigate('Thankspage', {
            id: id,
            name: productName,
            description: productDescription,
            price: productPrice,
            image: productImage
        })

    }

    render() {
        const id = this.props.navigation.getParam('id', 'NO-ID')
        const productName = this.props.navigation.getParam('name', 'NO-ID')
        const productDescription = this.props.navigation.getParam('description', 'NO-ID')
        const productPrice = this.props.navigation.getParam('price', 'NO-ID')
        const productImage = this.props.navigation.getParam('image', 'NO-ID')
        return (
            <View style={styles.container}>
                <Image source={require('../admin/images/image.png')} style={styles.image} />
                <Divider />
                <View style={{ padding: 16 }}>
                    <Text style={styles.textProduk}>
                        {productName}
                    </Text>
                    <Text style={styles.textHarga}>
                        {productPrice}
                    </Text>
                </View>
                <Divider />
                <ScrollView>
                    <View style={{ padding: 16, flex: 1 }}>
                        <Text style={styles.textDeksripsi}>Deskripsi</Text>
                        <Text style={styles.textIsi}>{productDescription}</Text>
                    </View>
                </ScrollView>
                <View style={{ padding: 16 }}>
                    <Button dark={true} mode="contained" color="tomato" uppercase={false} style={{ padding: 5 }}
                        onPress={this.btnAddToCart}>
                        Checkout
                    </Button>
                </View>
            </View>
        )
    }
}

// const mapDispatchToProps = state => {
//     return {
//         payload: state.products
//     }
// }

// export default connect(null, mapDispatchToProps)(Checkout)

const screen = Dimensions.get('window')
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    image: {
        height: 200,
        width: screen.width,
        alignSelf: 'center'
    },
    textProduk: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'black'
    },
    textHarga: {
        fontSize: 16,
        fontWeight: 'bold',
        color: 'red'
    },
    textDeksripsi: {
        fontSize: 14,
        fontWeight: 'bold',
        color: 'black'
    },
    textIsi: {
        marginTop: 8,
        fontSize: 14,
    },
})