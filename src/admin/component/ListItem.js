import React, { Component } from 'react';
import {
    View, StyleSheet, Text, Image, TouchableOpacity, ScrollView
} from 'react-native';
import { Divider } from 'react-native-paper';

const ListItem = ({ products, navigation }) => (

    <ScrollView>
        {products.map(product =>
            <TouchableOpacity activeOpacity={0.5} key={product.id}
                onPress={() => navigation.navigate('EditProduct', {
                    id: product.id,
                    name: product.name,
                    description: product.description,
                    price: product.price,
                    image: product.image
                })}>
                <View style={styles.container}>
                    <Image
                        style={styles.image}
                        source={{uri: JSON.stringify(product.image)}} />
                    <View style={{ flexDirection: 'column', flex: 1, paddingHorizontal: 8 }}>
                        <Text style={styles.textProduk}>
                            {product.name}
                        </Text>
                        <Text style={styles.textDeskripsi}>
                            {product.description}
                        </Text>
                        <Text style={styles.textHarga}>
                            Rp. {product.price}
                        </Text>
                    </View>
                </View>
                <Divider style={{ marginHorizontal: 16 }} />
            </TouchableOpacity>
        )}
    </ScrollView>
)

export default ListItem

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderRadius: 10,
        marginTop: 8,
        padding: 8,
        alignItems: 'center',
        justifyContent: 'center',
        marginHorizontal: 16
    },
    image: {
        borderWidth: 1,
        borderColor: 'white',
        height: 50,
        width: 50
    },
    textProduk: {
        fontSize: 16,
        color: 'gray',
        fontWeight: 'bold'
    },
    textDeskripsi: {
        fontSize: 14,
        color: 'gray'
    },
    textHarga: {
        alignItems: 'center',
        justifyContent: 'center',
        fontSize: 16,
        fontWeight: 'bold',
        color: 'gray',
    }
})