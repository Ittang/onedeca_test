import React, { Component } from 'react';
import {
    StyleSheet, View, ToastAndroid, Image, Text,
    ScrollView, TouchableOpacity, PixelRatio
} from 'react-native';
import { TextInput, Button } from 'react-native-paper';
import { connect } from 'react-redux';
import { addProduct } from '../../actions'
import ImagePicker from 'react-native-image-picker'

class AddProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
            productName: '',
            productDescription: '',
            productPrice: '',
            productImage: '',
            avatarSource: null
        }
    }

    browsePhoto = () => {
        const options = {
            noData: true,
            storageOption: {
                skipBackup: true
            }
        }
        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                ToastAndroid.show("Cancel", ToastAndroid.SHORT)
            } else if (response.error) {
                ToastAndroid.show("Error: "+response.error, ToastAndroid.SHORT)
            } else {
                const source = { uri: response.uri }
                this.setState({
                    avatarSource: source
                })
            }
        })
    }

    addProduct = () => {
        //
        const {
            productName, productDescription,
            productPrice, avatarSource
        } = this.state;
        if (productName.trim() == "") {
            ToastAndroid.show('Nama produk  kosong', ToastAndroid.SHORT)
        }
        else if (productDescription.trim() == "") {
            ToastAndroid.show('Deskripsi produk kosong', ToastAndroid.SHORT)
        }
        else if (productPrice.trim() == "") {
            ToastAndroid.show('Harga produk kosong', ToastAndroid.SHORT)
        }
        else if (avatarSource == null) {
            ToastAndroid.show('Gambar produk kosong', ToastAndroid.SHORT)
        }
        else {
            // convertToRupiah = (angka) => {
            //     let rupiah = ''
            //     let angkarev = angka.toString().split('').reverse().join('')
            //     for (let i = 0; i < angkarev.length; i++) if(i%3==0) rupiah += angkarev.substr(i, 3) + '.'
            //     return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('')
            // }
            this.props.dispatch(addProduct(
                productName, 
                productDescription, 
                productPrice, 
                avatarSource
            ))
            ToastAndroid.show('Data disimpan', ToastAndroid.SHORT)
            this.props.navigation.goBack()
        }
    }

    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <TouchableOpacity onPress={this.browsePhoto}>
                        <View style={[styles.avatarContainer, styles.avatar]}>
                            {this.state.avatarSource === null ?
                                (<Text>Select a photo</Text>) :
                                (<Image style={styles.avatar} source={this.state.avatarSource} />)
                            }
                        </View>
                    </TouchableOpacity>
                    <TextInput
                        onChangeText={(text) => this.setState({ productName: text })}
                        placeholder="Nama produk"
                        mode="outlined" />
                    <TextInput
                        onChangeText={(text) => this.setState({ productDescription: text })}
                        placeholder="Deskripsi produk"
                        mode="outlined" />
                    <TextInput
                        keyboardType="numeric"
                        onChangeText={(text) => this.setState({ productPrice: text })}
                        placeholder="Harga produk"
                        mode="outlined" />
                    <View style={styles.imageContainer}>
                        <Image style={styles.imageAvatar} source={this.state.avatarSource} />
                    </View>
                    <Button dark={true} uppercase={false} color="tomato" mode="contained" style={{ marginTop: 10, padding: 5 }}
                        onPress={this.addProduct}>
                        Tambah
                    </Button>
                </View>
            </ScrollView>
        )
    }
}

export default connect()(AddProduct)

const styles = StyleSheet.create({
    container: {
        shadowRadius: 10,
        justifyContent: 'center',
        padding: 16
    },
    addProductText: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    avatarContainer: {
        borderColor: 'gray',
        borderWidth: 1 / PixelRatio.get(),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    avatar: {
        borderRadius: 75,
        height: 150,
        width: 150
    },
    button: {
        marginTop: 8,
        borderRadius: 10,
        backgroundColor: '#999'
    },
})