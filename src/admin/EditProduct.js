import React, { Component } from 'react'
import {
    View, StyleSheet, TouchableOpacity, Image,
    Text, ToastAndroid, ScrollView, Alert, PixelRatio
} from 'react-native'
import { TextInput, Button } from 'react-native-paper'
import ImagePicker from 'react-native-image-picker'
import { connect } from 'react-redux'
import { updateProduct, removeProduct } from '../actions'

const options = {
    title: 'Upload image',
    storageOptions: {
        skipBackup: true,
        path: 'images'
    }
}

class EditProduct extends Component {
    constructor(props) {
        super(props)
        this.state = {
            inputProductName: '',
            inputProductDescription: '',
            inputProductPrice: '',
            inputProductImage: ''
        }
    }

    browsePhoto = () => {
        ImagePicker.launchImageLibrary(options, (response) => {
            if (response.didCancel) {
                ToastAndroid.show("Cancel", ToastAndroid.SHORT)
            } else if (response.error) {
                ToastAndroid.show("Error: "+response.error, ToastAndroid.SHORT)
            } else {
                const source = { uri: response.uri }
                this.setState({
                    avatarSource: source
                })
            }
        })
    }

    updateProduct = () => {
        // convertToRupiah = (angka) => {
        //     let rupiah = ''
        //     let angkarev = angka.toString().split('').reverse().join('')
        //     for (let i = 0; i < angkarev.length; i++) if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.'
        //     return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('')
        // }
        const productId = this.props.navigation.getParam('id', 'NO-ID')
        const newProductName = this.state.inputProductName
        const newProductDescription = this.state.inputProductDescription
        const newProductPrice = this.state.inputProductPrice
        const newProductImage = this.state.inputProductImage
        const data = {
            newProductName, newProductDescription,
            newProductPrice, newProductImage
        }
        if (newProductName == "" && newProductDescription == "" && newProductPrice == "") {
            ToastAndroid.show('Tidak ada perubahan yang disimpan', ToastAndroid.SHORT)
            this.props.navigation.goBack()
        }
        else if (newProductName == "") {
            ToastAndroid.show("Nama produk kosong", ToastAndroid.SHORT)
        }
        else if (newProductDescription == "") {
            ToastAndroid.show("Deskripsi produk kosong", ToastAndroid.SHORT)
        }
        else if (newProductPrice == "") {
            ToastAndroid.show("Harga produk kosong", ToastAndroid.SHORT)
        }
        else {
            this.props.dispatch(updateProduct(productId, data))
            ToastAndroid.show('Succesfully updated ', ToastAndroid.SHORT)
            this.props.navigation.goBack()
        }

    }

    removeProduct = () => {
        const productId = this.props.navigation.getParam('id', 'NO-ID')
        const productName = this.props.navigation.getParam('name', 'NO-ID')
        Alert.alert(
            'Confirm delete',
            'Are you sure want to delete ' + productName + '?',
            [
                { text: 'No', onPress: console.log("Cancel delete"), style: 'cancel' },
                {
                    text: 'Yes', onPress: () => {
                        this.props.dispatch(removeProduct(productId))
                        ToastAndroid.show('Succesfully delete', ToastAndroid.SHORT)
                        this.props.navigation.goBack()
                    }
                },
                { cancelable: true }
            ]
        )
    }

    render() {
        const productName = this.props.navigation.getParam('name', 'NO-ID')
        const productDescription = this.props.navigation.getParam('description', 'NO-ID')
        const productPrice = this.props.navigation.getParam('price', 'NO-ID')
        const productImage = this.props.navigation.getParam('image', 'NO-ID')
        return (
            <ScrollView>
                <View style={styles.container}>
                    <TouchableOpacity onPress={this.browsePhoto}>
                        <View style={[styles.avatarContainer, styles.avatar]}>
                            {this.state.avatarSource === null ?
                                (<Text>Select a photo</Text>) :
                                (<Image style={styles.avatar} source={this.state.avatarSource} />)
                            }
                        </View>
                    </TouchableOpacity>
                    <TextInput
                        placeholder={productName}
                        onChangeText={(text) => this.setState({ inputProductName: text })}
                        mode="outlined" />
                    <TextInput
                        placeholder={productDescription}
                        onChangeText={(text) => this.setState({ inputProductDescription: text })}
                        mode="outlined" />
                    <TextInput
                        keyboardType="numeric"
                        placeholder={productPrice}
                        onChangeText={(text) => this.setState({ inputProductPrice: text })}
                        mode="outlined" />
                    <View style={styles.imageContainer}>
                        <Image style={styles.imageAvatar} />
                    </View>
                    <Button uppercase={false} color="tomato" dark={true} mode="contained"
                        style={{ marginTop: 10, padding: 5 }}
                        onPress={this.updateProduct}>
                        Simpan
                    </Button>
                    <Button uppercase={false} mode="outlined" color="tomato" style={{ marginTop: 10, padding: 5, borderColor: "tomato" }}
                        onPress={this.removeProduct}>
                        Hapus
                    </Button>
                </View>
            </ScrollView>
        )
    }
}

export default connect()(EditProduct)

const styles = StyleSheet.create({
    container: {
        shadowRadius: 10,
        justifyContent: 'center',
        padding: 16
    },
    addProductText: {
        fontSize: 24,
        fontWeight: 'bold'
    },
    avatarContainer: {
        borderColor: 'blue',
        borderWidth: 1 / PixelRatio.get(),
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    avatar: {
        borderRadius: 75,
        height: 150,
        width: 150
    },
    button: {
        marginTop: 8,
        borderRadius: 10,
        backgroundColor: '#999'
    },
})