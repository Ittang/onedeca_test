import React, { Component } from 'react';
import {
    View, Text, StyleSheet, TouchableOpacity,
    AsyncStorage, ToastAndroid, Alert
} from 'react-native';
import { Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Feather';

import VisibleProducts from '../containers/VisibleProducts';

export default class Dashboard extends Component {

    componentDidMount = () => {
        AsyncStorage.getItem('admin:username').then((value) => this.setState({ username: value }))
    }

    state = {
        products: [],
        visibilityFilter: "SHOW_ALL_PRODUCTS"
    }

    constructor(props) {
        super(props)
        this.state = {
            username: ''
        }
    }

    addProduct = () => {
        this.props.navigation.navigate('AddProduct')
    }

    logout = () => {
        Alert.alert(
            'Confirm logout',
            'Are you sure want to logout?',
            [
                { text: 'No', onPress: console.log("Cancel delete"), style: 'cancel' },
                {
                    text: 'Yes', onPress: () => {
                        ToastAndroid.show('Logged out', ToastAndroid.SHORT)
                        this.props.navigation.navigate('LoginScreen')
                    }
                },
                { cancelable: true }
            ]
        )
    }

    render() {
        return (
            <View style={{ flex: 1, paddingBottom: 8 }}>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={styles.welcomeText}>Welcome back, {this.state.username}!</Text>
                    <TouchableOpacity activeOpacity={0.3} onPress={() => this.props.navigation.navigate('Payment')}>
                        <Icon name="shopping-cart" size={24} style={styles.icon} />
                    </TouchableOpacity>
                    <TouchableOpacity activeOpacity={0.3} onPress={this.logout}>
                        <Icon name="log-out" size={24} style={[styles.icon, { paddingLeft: 8 }]} />
                    </TouchableOpacity>
                </View>
                <TouchableOpacity activeOpacity={0.5} onPress={this.addProduct}>
                    <Button uppercase={false} icon="add" color="white" style={styles.button}>
                        Tambah produk
                    </Button>
                </TouchableOpacity>
                <VisibleProducts navigation={this.props.navigation} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    welcomeText: {
        flex: 1,
        marginTop: 16,
        marginLeft: 16,
        fontWeight: 'bold',
        fontSize: 18,
    },
    button: {
        marginHorizontal: 16,
        marginTop: 8,
        borderRadius: 10,
        backgroundColor: '#999'
    },
    icon: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 16
    }
})