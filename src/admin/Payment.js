import React, {Component} from 'react';
import {
    View, Text, StyleSheet
} from 'react-native';

export default class Payment extends Component {
    render(){
        return(
            <View style={styles.container}>
                <Text>Payment</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems:'center',
        justifyContent: 'center'
    }
})