import { connect } from 'react-redux'
import ListItem from '../admin/component/ListItem'
import { toggleProduct } from '../actions'

const mapStateToProp = state => ({
    products: state.products
})

const mapDispatchToProps = dispatch => ({
    toggleProduct: id => dispatch(toggleProduct(id))
})

export default connect(mapStateToProp, mapDispatchToProps)(ListItem)